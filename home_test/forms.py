from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from home_test.models import Profile

class FilterForm(forms.Form):
    rank_choices = (
            ("a", "1 star"),
            ("b", "2 star"),
            ("c","3 star"),
            ("d","4 star"),
            ("e","5 star")
            )
    ranks= forms.MultipleChoiceField(widget=forms.RadioSelect(),
                                         choices=rank_choices)
    
    # countr = Profile.objects.filter(solved__contains=['country'])
    # print (countr)

    # def get_my_choices():
    #     # you place some logic here
    #     countr = Profile.objects.filter(solved__contains=['country'])
    #     print (countr)
    #     return countr

    # my_choice_field = forms.ChoiceField(choices=countr)

class ProbmSearchForm(forms.Form):
    probcode=   forms.CharField(required=True, widget=forms.TextInput(attrs={'placeholder': 'Enter the problem name or code...','class':'form-control form-control-lg'}))


# class SignUpForm(UserCreationForm):
#     email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')
#
#     class Meta:
#         model = User
#         fields = ('username', 'email', 'password1', 'password2', )


class UploadFileForm(forms.Form):
    
    PROGRAM = (
       ("cpp", ("C++")),
       ("py", ("Python")), 
       ("c", ("C")),
    )
    bruteforce=forms.FileField()
    brute = forms.ChoiceField(choices=PROGRAM,label="Select the programming language in which you have written brute force",required=True)
    optimize=forms.FileField()
    opti = forms.ChoiceField(choices=PROGRAM,label="Select the programming language in which you have written optimized code",required=True)
    

    def clean_opti(self):
      
      code_opti=self.cleaned_data.get("opti")		
      opti_file=self.cleaned_data.get("optimize")
      
      if ("." + code_opti) not in opti_file.name: 
        raise forms.ValidationError("Please enter write the file with write extension")
      return code_opti



    def clean_brute(self):
      code_bru=self.cleaned_data.get("brute")
      bru_file=self.cleaned_data.get("bruteforce")
      if ("." + code_bru) not in bru_file.name: 
        raise forms.ValidationError("Please enter write the file with write extension")
      return code_bru