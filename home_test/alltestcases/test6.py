import string
import random

def id_generator(size=3, chars=string.ascii_lowercase):
        return ''.join(random.choice(chars) for _ in range(size))

def generate(number):
        open("arraygen.txt", "w").close()
        file = open('arraygen.txt','a')
        file.write(str(number)+'\n')
        if number>20000:
                number=random.randint(1,20000)
        for i in range(number):
                file.write(str(id_generator())+'\n')
                file.write(str(id_generator())+'\n')
        file.close()
        return(number,number,2)

