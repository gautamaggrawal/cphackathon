from test1 import Rand,unique
import random
import os

def generate(tc,n,x,s):
    if tc>=1000000:
        tc=random.randint(0,100000)
    if n>=100000:
        n=random.randint(0,100000)
    if x>=100000:
        x=random.randint(0,100000)
    if s>=10000:
        s=random.randint(0,10000)
    open("arraygen.txt", "w").close()
    file = open('arraygen.txt','a')
    file.write(str(tc)+'\n')
    for i in range(0,tc):
        n=random.randint(2,n)
        x=random.randint(1,n)
        s=random.randint(1,s)
        file.write(str(n)+' '+str(x)+' '+str(s)+' '+'\n')
        for j in range(s):
            list1=(Rand(1,n,2))
            finalans=unique(list1,2,1,n)
            st1=" ".join(str(g) for g in finalans)
            file.write(st1+'\n')
    file.close()
    return(tc,tc,1)
    