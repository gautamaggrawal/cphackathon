from test1 import Rand,unique
import random
import os

def generate(tc,n,minv,maxv):
    if tc>=10:
        tc=random.randint(0,10)
    if n>=100000:
        n=random.randint(0,100000)
    if maxv<=minv:
        minv=1
        maxv=random.randint(1,1000000)
    if maxv>=1000000:
        maxv=random.randint(1,1000000)
    open("arraygen.txt", "w").close()
    file = open('arraygen.txt','a')
    file.write(str(tc)+'\n')
    for i in range(0,tc):
        n=random.randint(1,n)
        file.write(str(n)+'\n')
        list1=Rand(minv,maxv,n)
        st=" ".join(str(x) for x in list1)
        file.write(st+'\n')
    file.close()
    return(tc,tc,1)