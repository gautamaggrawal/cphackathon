import random
import os

def generate(tc,n,m,x,y):
    if tc>=1000:
        tc=random.randint(0,1000)
    if n>=1000000000:
        n=random.randint(0,10000000000)
    if m>=10000000000:
        m=random.randint(0,10000000000)
    if x>=10000000000:
        x=random.randint(0,10000000000)
    if y>=10000000000:
        y=random.randint(0,10000000000)
    open("arraygen.txt", "w").close()
    file = open('arraygen.txt','a')
    file.write(str(tc)+'\n')
    for i in range(0,tc):
        n=random.randint(2,n)
        m=random.randint(1,m)
        x=random.randint(1,x)
        y=random.randint(1,y)
        file.write(str(n)+' '+str(m)+' '+str(x)+' '+str(y)+'\n')
    file.close()
    return(tc,tc,1)
