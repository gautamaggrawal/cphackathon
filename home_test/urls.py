from django.conf.urls import url,include
from . import views

app_name='home_test'
urlpatterns = [
    url('^$',views.home1,name="home"),
    url('^logout/$',views.logout_user,name="logout"),
    url('^myprofile/$',views.profile,name="profile"),
 	url('^cp/login_user/$',views.login_user,name="login_user"),
 	url('^cp/login/$',views.loginView,name="login"),
 	url('^testcase/gogenerate/$',views.GenerateTest.as_view(),name="GenerateTest"),
 	url('^tc/$',views.tc,name="tc"),
 	url('^home/$',views.home,name="home1"),
 	url(r'^src/OAuth2/',views.verify,name='verify'),
	url(r'^home/problemsearch/(?P<pcode>\w+)/',views.search,name='search')
]
