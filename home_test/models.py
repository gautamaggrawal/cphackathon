from django.db import models
from django.contrib.auth.models import User


class Profile(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    rating = models.CharField(max_length=50,null=True)
    rank=models.CharField(max_length=50,null=True)
    todo=models.TextField(default="",null=True)
    country=models.CharField(max_length=50,blank=True,null=True)
    points=models.CharField(max_length=10,default="",null=True)
    scope=models.TextField(default="")
    access_token=models.CharField(max_length=500,default="")
    refresh_token=models.CharField(max_length=500,default="")

    def _str_(self):
        return str(self.user)

class Tags(models.Model):
    tag_name=models.CharField(max_length=200,null=True)

class Problems(models.Model):
    prob_name=models.CharField(max_length=200)
    prob_code=models.CharField(max_length=200,null=True)
    prob_tag=models.TextField(null=True)

    def __str__(self):
        return str(self.prob_name)
        
class UserSolved(models.Model):
    user=models.ForeignKey(Profile,on_delete=models.CASCADE)
    problem=models.ForeignKey(Problems,on_delete=models.CASCADE)
    solved=models.IntegerField(default=1)
    