from urllib.request import Request, urlopen
from bs4 import BeautifulSoup
from django.views.decorators.csrf import csrf_protect

import json
from home_test.models import Profile,Tags,Problems,UserSolved
from home_test.taskamcp import realwork
from home_test.forms import UploadFileForm,ProbmSearchForm,FilterForm
from django.contrib.auth import login,logout
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from home_test.alltestcases import test6,test7,test8,test9,test10,testprince,cootest
from home_test.alltestcases import sep1,sep2,sep3,sep5,sep6,sep7,sep8
import requests
from urllib.request import Request, urlopen
import os
from django.conf import settings
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt


def login_user(request):   
    return redirect("https://api.codechef.com/oauth/authorize?response_type=code&client_id=d15bf6e8dec10573d58aacda8b2c25e5&state=xyz&redirect_uri=http://149.129.128.10/src/OAuth2/")

def loginView(request):
    form = UploadFileForm()
    return render(request,'home_test/index.html',{'form':form,'name' : request.user.username})

def logout_user(request):
    logout(request)
    return redirect('/')

@csrf_exempt
def home(request):
    # url="https://www.codechef.com/problems/easy"
    # #url="https://www.codechef.com/problems/school"
    # #url="https://www.codechef.com/problems/medium"
    # #url="https://www.codechef.com/problems/hard/"
    # # url="https://www.codechef.com/problems/challenge/"
    
    # req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
    # web_byte = urlopen(req).read()
    # html_doc = web_byte.decode('utf-8')
    # soup = BeautifulSoup(html_doc, 'html.parser')
    # all_content=soup.find('table',{"class":"dataTable"})
    # #print(all_content)
    # '''for problems in soup.find_all('b'):
    #     print(problems.string)'''

    # for link in all_content.find_all('a'):
        
    #     x=str(link.get('href'))
    #     if x.startswith('/problems'):
    #         y=str(link.get('href'))
    #         #print(link.text)
    #         d=(y.split('/problems/'))
    #         Problems.objects.create(prob_name=str(link.text),prob_code=str(d[1]))
    
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)

        if form.is_valid():            
            op=form.cleaned_data.get("opti")
            br=form.cleaned_data.get("brute")
            bru=request.FILES['bruteforce']
            opi=request.FILES['optimize']
            realwork(bru,opi,br,op)
            return HttpResponse("Done")
        else:
            form = UploadFileForm(request.POST, request.FILES)
            return render(request, 'home_test/index.html', {'form': form})
    else:
        form = UploadFileForm()
        return render(request, 'home_test/index.html', {'form':form,})

def tc(request):
    return render(request, 'home_test/base.html')

def home1(request):
    probform = ProbmSearchForm()
    if request.method=="POST":
        probform = ProbmSearchForm(request.POST)
        if probform.is_valid():
            pcode = probform.cleaned_data.get("probcode")
            return redirect('/home/problemsearch/'+str(pcode)+'/')
        else:
            probform =ProbmSearchForm(request.POST)
            return render(request, 'home_test/home.html', {'probform': probform})
    
    return render(request, 'home_test/home.html',{'probform':probform})

def profile(request):
    prob=Problems.objects.all() 
    prof = Profile.objects.filter(user=User.objects.get(username=request.user))
    for e in prof:
        username=e.user
        rating=e.rating
        rank=e.rank
        todo=e.todo
        country=e.country
        points=e.points
    user_solved=UserSolved.objects.filter(user=Profile.objects.get(user=request.user),solved=1).prefetch_related("problem").values_list("problem__prob_name","problem__prob_code")
    user_unsolved=UserSolved.objects.filter(user=Profile.objects.get(user=request.user),solved=0).prefetch_related("problem").values_list("problem__prob_name","problem__prob_code")
    return render(request, 'home_test/profile.html',{"username":username,"rating":rating,"rank":rank,"todo":todo,'country':country,'points':points,'solved':user_solved,'unsolved':user_unsolved,'solcount':len(user_solved)})

def search(request,pcode):

    users_solved=UserSolved.objects.filter(problem__prob_code=pcode)
    user_unsolved=UserSolved.objects.filter(user=Profile.objects.get(user=request.user),solved=0).prefetch_related("problem").values_list("problem__prob_name","problem__prob_code")
    print(users_solved)
    if users_solved.exists():
        return render(request,'home_test/usersearch.html',{"all_users":users_solved,"unsolved":user_unsolved,"pcode":pcode})
    else:
        return HttpResponse("Oops the problem code is incorrect or no users for this problem .Click <a href='/'>Here</a> to go back." )
    
    

# def tyc(request):
#     url="https://www.codechef.com/problems/easy"
#     url="https://www.codechef.com/problems/school"
#     url="https://www.codechef.com/problems/medium"
#     url="https://www.codechef.com/problems/hard/"
#     url="https://www.codechef.com/problems/challenge/"
    
#     req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
#     web_byte = urlopen(req).read()
#     html_doc = web_byte.decode('utf-8')
#     soup = BeautifulSoup(html_doc, 'html.parser')
#     all_content=soup.find('table',{"class":"dataTable"})
#     #print(all_content)
#     '''for problems in soup.find_all('b'):
#         print(problems.string)'''

#     for link in all_content.find_all('a'):
        
#         x=str(link.get('href'))
#         if x.startswith('/problems'):
#             y=str(link.get('href'))
#             print(link.text)
#             d=(y.split('/problems/'))
#             Problems.objects.create(prob_name=str(link.text),prob_code=str(d[1]))



#     return render(request, 'home_test/testyourcode.html')


def verify(request):
    res=requests.post("https://api.codechef.com/oauth/token",dict({"grant_type":"authorization_code","code":request.GET['code'],"client_id":"d15bf6e8dec10573d58aacda8b2c25e5","client_secret":"f468cc8174b59932f6d6c9a33a6fa21e","redirect_uri":"http://149.129.128.10/src/OAuth2/"}))
    try:
        res=json.loads(res.text)
        if "result" in res:
            access_token=res.get("result").get("data").get("access_token")
            refresh_token=res.get("result").get("data").get("refresh_token")
            scope=res.get("result").get("data").get("scope")
            res_username=requests.get("https://api.codechef.com/users/me",headers={"Authorization":"Bearer "+str(access_token)})
            response=json.loads(res_username.text)
            response=response.get("result").get("data")
            print(response)            
            country=response.get("content").get("country").get("name")
            rank=response.get("content").get("rankings").get("allContestRanking").get("global")
            rating=response.get("content").get("ratings").get("allContest")
            band=response.get("content").get("band")
            solved_problem=response.get("content").get("problemStats").get("solved")
            partially_solved=response.get("content").get("problemStats").get("partiallySolved")

            exists=User.objects.filter(username=response.get("content").get("username")).exists()
            if exists:
                user=User.objects.get(username=response.get("content").get("username"))
            else:
                user=User.objects.create(username=response.get("content").get("username"),first_name=response.get("content").get("fullname"))
                Profile.objects.create(access_token=access_token, refresh_token=refresh_token, scope=scope.split(" "),user=user,country=country,rank=rank,rating=rating,points=band)

            profile_obj=Profile.objects.get(user=user)
            for key in solved_problem:
                for items in solved_problem[key]:
                    try:
                        prob=Problems.objects.get(prob_code=items)
                        UserSolved.objects.get_or_create(problem=prob,user=profile_obj)
                    except Exception as e:
                        pass

            for key in partially_solved:
                for items in partially_solved[key]:
                    try:
                        prob=Problems.objects.get(prob_code=items)
                        UserSolved.objects.get_or_create(problem=prob,user=profile_obj,solved=0)
                    except Exception as e:
                        pass

            login(request,user)
            return redirect("/")
        else:
            return HttpResponse(str(res))
    except Exception as e:
        return HttpResponse("Something went wrong try again. Click <a href='/'>Here</a> to go back." )

class GenerateTest(APIView):
    def post(self,request,*args,**kwargs):
        select=request.data.get('select')
        ques_id=request.data.get("id")
        if ques_id=='sep1':
            testcase=request.data.get("testcase")
            N=request.data.get("N")
            X=request.data.get("X")
            S=request.data.get("S")
            x,y,nol=sep1.generate(int(testcase),int(N),int(X),int(S))

        if ques_id=='sep2':
            testcase=request.data.get("testcase")
            N=request.data.get("N")
            M=request.data.get("M")
            X=request.data.get("X")
            Y=request.data.get("X")
            x,y,nol=sep2.generate(int(testcase),int(N),int(M),int(X),int(Y))

        if ques_id=='sep3':
            testcase=request.data.get("testcase")   
            N=request.data.get("N")
            minv=request.data.get("minv")
            maxv=request.data.get("maxv")
            x,y,nol=sep3.generate(int(testcase),int(N),int(minv),int(maxv))

        if ques_id=='sep5':
            testcase=request.data.get("testcase")
            N=request.data.get("N")
            M=request.data.get("M")
            Q=request.data.get("Q")
            x,y,nol=sep6.generate(int(testcase),int(N),int(M),int(Q))

        if ques_id=='sep6':
            N=request.data.get("N")
            M=request.data.get("M")
            K=request.data.get("K")
            sep7.generate(int(N),int(M),int(K))

        if ques_id=='sep7':
            testcase=request.data.get("testcase")
            N=request.data.get("N")
            arrsize=request.data.get("M")
            Q=request.data.get("Q")
            x,y,nol=sep8.generate(int(testcase),int(N),int(Q),int(arrsize))

        if ques_id=='1':
            testcase=request.data.get("testcase")
            x,y,nol=test6.generate(int(testcase))
        if ques_id=='2':
            testcase=request.data.get("testcase")
            maxn=request.data.get("N")
            x,y,nol=test7.generate((int(testcase)),(int(maxn)))
        if ques_id=='3':
            testcase=request.data.get("P")
            maxn=request.data.get("S")
            x,y,nol=test8.generate((int(testcase)),(int(maxn)))
        if ques_id=='4':
            testcase=request.data.get("testcase")
            x,y,nol=test9.generate((int(testcase)))
        if ques_id=='5':
            testcase=request.data.get("testcase")
            maxn=request.data.get("N")
            maxp=request.data.get("P")
            maxai=request.data.get("max")
            minai=request.data.get("min")
            x,y,nol=testprince.generate((int(testcase)),(int(maxp)),int(maxn),int(maxai),int(minai))
        if ques_id=='8':
            testcase=request.data.get("testcase")
            maxn=request.data.get("N")
            maxp=request.data.get("S")
            maxai=request.data.get("max")
            minai=request.data.get("min")
            x,y,nol=cootest.generate(int(testcase),int(maxn),int(maxp),int(maxai),int(minai))
        
        if select == 'compare':
            bru = request.FILES.getlist('brute[]')[0]
            opi = request.FILES.getlist('opti[]')[0]
            br= request.POST.get("exbrute")
            op=request.POST.get("exopti")
            email_user=request.POST.get("email_user")
            realwork(bru,opi,br,op,x,y,nol,email_user)
        return Response({"msg":"done"},status=status.HTTP_200_OK)
