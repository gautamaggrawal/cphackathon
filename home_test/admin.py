from django.contrib import admin

from .models import Problems,Profile,Tags,UserSolved

admin.site.register(Profile)
admin.site.register(Problems)
admin.site.register(Tags)
admin.site.register(UserSolved)


