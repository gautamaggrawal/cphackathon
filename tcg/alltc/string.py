import string

from test1 import Rand

import os
import random

def id_generator(size, chars):
        return ''.join(random.choice(chars) for _ in range(size))


def generate(tc,si,cha):
        if not os.path.exists(os.getcwd()+'/media'):
                os.makedirs(os.getcwd()+'/media')
        ddd=os.getcwd()+'/media/string.txt'
        if tc>=1000000:
                tc=random.randint(0,99999)

        open(ddd, "w").close()
        file = open(ddd,'a')
        for i in range(tc):
                file.write(str(id_generator(si,cha))+'\n')
        file.close()

