from test1 import Rand,unique
import random
import os
def generate(tc,arrsize,minv,maxv,distn):
        if not os.path.exists(os.getcwd()+'/media'):
                os.makedirs(os.getcwd()+'/media')
        ddd=os.getcwd()+'/media/array.txt'
        open(ddd, "w").close()
        file = open(ddd,'a')
        for i in range(0,tc):
                if distn=='NO':
                        finalans=(Rand(minv,maxv,arrsize))
                else:
                        list1=(Rand(minv,maxv,arrsize))
                        finalans=unique(list1,arrsize,minv,maxv)
                st=" ".join(str(x) for x in finalans)
                file.write(st+'\n')
        file.close()
