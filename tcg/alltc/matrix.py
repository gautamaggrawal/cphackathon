from test1 import Rand,unique
import random
import os

def generate(tc,row,col,minv,maxv,distn):
        if tc>1000000:
                tc=random.randint(0,100000)
        if row>1000000:
                row=random.randint(0,100000)
        if col>1000000:
                col=random.randint(0,100000)
        if minv>maxv or maxv<minv:
                print("check input values")
                
        if not os.path.exists(os.getcwd()+'/media'):
                os.makedirs(os.getcwd()+'/media')
        ddd=os.getcwd()+'/media/matrix.txt'
        open(ddd, "w").close()
        file = open(ddd,'a')
        for i in range(0,tc):
                for j in range(0,row):
                        if distn=='NO':
                                finalans=(Rand(minv,maxv,col))
                        else:
                                list1=(Rand(minv,maxv,col))
                                finalans=unique(list1,col,minv,maxv)
                        st=" ".join(str(x) for x in finalans)
                        file.write(st+'\n')
                file.write('\n')
        file.write('\n')
        file.close()


generate(51,5,5,1,11,'n')