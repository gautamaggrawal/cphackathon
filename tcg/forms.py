from django import forms

class NumberForm(forms.Form):
    testcase=forms.IntegerField(required =True)
    minv=forms.IntegerField(required=True,max_value=9999,min_value=0)
    maxv=forms.IntegerField(required=True,max_value=100000,min_value=0)
    unique=forms.BooleanField(required=False)


    def clean(self):
        cleaned_data = super(NumberForm, self).clean()
        testcase = self.cleaned_data.get('testcase')
        minv = self.cleaned_data.get('minv')
        maxv = self.cleaned_data.get('maxv')
        if minv==None or maxv==None:
            raise forms.ValidationError("Values can't be empty")
    
        if minv>=maxv:
            raise forms.ValidationError('Minv value cannot exceed Maxv value  ')

class MatrixForm(forms.Form):
    Testcase=forms.IntegerField(required =True,max_value=100000,min_value=1)
    Minv=forms.IntegerField(required=True,max_value=9999,min_value=0)
    Maxv=forms.IntegerField(required=True,max_value=100000,min_value=0)
    unique=forms.BooleanField(required=False)
    rows=forms.IntegerField(required=True,max_value=9999,min_value=0)
    col=forms.IntegerField(required=True,max_value=100000,min_value=0)

class ArrayForm(forms.Form):
    Testcase=forms.IntegerField(required =True,max_value=10000,min_value=1)
    minin=forms.IntegerField(required=True,max_value=9999,min_value=0)
    maxin=forms.IntegerField(required=True,max_value=100000,min_value=0)
    unique=forms.BooleanField(required=False)
    arrsize=forms.IntegerField(required=True,max_value=100000,min_value=0)

class StringForm(forms.Form):
    Testcas=forms.IntegerField(required =True)
    StringLength=forms.IntegerField(required =True)
    CharactersAllowed=forms.CharField(required=True)
    # OPTIONS = (
    #         ("a", "A-Z"),
    #         ("b", "a-z"),
    #         ("c","0-9")
    #         )
    # CharactersAllowed= forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple,
    #                                      choices=OPTIONS)




    

            


