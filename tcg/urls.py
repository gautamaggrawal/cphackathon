from django.conf.urls import *

from . import views


app_name='tcg'
urlpatterns = [
    url('^tcg/$',views.tcg,name="tcg1"),
    url('^tcg/numbers/$',views.number,name="numbers"),
    url('^tcg/matrix/$',views.matrixgen,name="matrix"),
    url('^tcg/array/$',views.arraygen,name="array"),
    url('^tcg/string/$',views.stringgen,name="string"),
    url('^download/(?P<file_name>.+)$', views.download,name='download'),

    # url('^tcg/static/tcg/lifecoder_1/matrix.txt/$',views.abc,name="abc")


    ]
# if settings.DEBUG:
#     # static files (images, css, javascript, etc.)
#     urlpatterns += patterns('',
#         (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
#         'document_root': settings.MEDIA_ROOT}))

# if settings.DEBUG:
#     urlpatterns = patterns('',
#     url(r'^media/(?P<path>.*)$', 'django.views.static.serve',{'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
# ) + urlpatterns

    
