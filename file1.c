#include<iostream>
using namespace std;
int main()
{
    int t;
    cin>>t;
    int ar[100000];
    while(t--)
    {
        int n;
        int checker[1000001]={0};
        cin>>n;
        int oddcount=0,evencount=0;
        int i;
        for(i=0;i<n;i++)
        {
            cin>>ar[i];
            checker[ar[i]]++;
            if(ar[i]%2==0)
                evencount++;
            else
                oddcount++;
        }
        int ans=0;
        ans=(evencount*(evencount-1))/2;
        ans+=(oddcount*(oddcount-1))/2;
        for(i=0;i<1000001;i++)
        {
            if(checker[i]>1)
            {
                int to;
                to=(checker[i]*(checker[i]-1))/2;
                ans=ans-to;
            }
        }
        for(i=0;i<1000001;i+=4)
        {
        	if(checker[i]>0)
            	ans=ans-checker[i+2]*checker[i];
        }
        for(i=1;i<1000001;i+=4)
        {
        	if(checker[i]>0)
            ans=ans-checker[i+2]*checker[i];
        }
        cout<<ans<<"\n";
    }
    return 0;
}
